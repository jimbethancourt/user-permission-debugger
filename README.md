## Stash User Permission Debugger

Small tool for debugging permissions granted to Stash users.

- Install via the UPM.
- Login as an *Administrator* and browse to ````<stash_url>/plugins/servlet/users-with/LICENSED_USER````

The response will be a plain text list of users granted the ````LICENSED_USER```` permission.

e.g.

    test
    admin
    user

You may substitute LICENSED_USER with any other ````com.atlassian.stash.user.Permission```` value, e.g. ````ADMIN````, ````SYS_ADMIN```` or ````PROJECT_CREATE````

For information about how each user is granted the permission, see the ````atlassian-stash.log```` for the period when you made the request. This plugin temporarily overrides the log level for ````PermissionServiceImpl```` which should result in some log output similar to:

    c.a.s.i.user.PermissionServiceImpl Switching to log level [TRACE]
    c.a.s.i.user.PermissionServiceImpl Calculating users with the LICENSED_USER permission
    c.a.s.i.user.PermissionServiceImpl 'admin' is explicitly granted LICENSED_USER
    c.a.s.i.user.PermissionServiceImpl 'TEST' is granted LICENSED_USER due to membership in group 'stash-users'
    c.a.s.i.user.PermissionServiceImpl 'user' is granted LICENSED_USER due to membership in group 'stash-users'
    c.a.s.i.user.PermissionServiceImpl 3 users have the LICENSED_USER permission
    c.a.s.i.user.PermissionServiceImpl user = com.atlassian.stash.internal.user.StashUserAuthenticationToken@586034f: Principal: InternalStashUser{id=1, name='admin'}; Credentials: [PROTECTED]; Authenticated: true; Details: null; Not granted any authorities
    c.a.s.i.user.PermissionServiceImpl projectId = null
    c.a.s.i.user.PermissionServiceImpl requested permission = ADMIN
    c.a.s.i.user.PermissionServiceImpl admin: ADMIN, or an inheriting permission, has been explicit granted
    c.a.s.i.user.PermissionServiceImpl Switching to log level [INFO]

