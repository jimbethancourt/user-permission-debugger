package com.atlassian.stash.plugins.licensed;

import com.atlassian.bitbucket.log.LoggingService;
import com.atlassian.bitbucket.permission.Permission;
import com.atlassian.bitbucket.permission.PermissionService;
import com.atlassian.bitbucket.permission.PermissionValidationService;
import com.google.common.collect.Lists;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PermissionServlet extends HttpServlet {

    private final static Pattern PATH_INFO = Pattern.compile("/([^/]+)/?");
    public static final String PERMISSION_SERVICE_LOGGER = "com.atlassian.stash.internal.user.PermissionServiceImpl";

    private final PermissionValidationService permissionValidationService;
    private final PermissionService permissionService;
    private final LoggingService loggingService;

    public PermissionServlet(PermissionValidationService permissionValidationService,
                             PermissionService permissionService, LoggingService loggingService) {
        this.permissionValidationService = permissionValidationService;
        this.permissionService = permissionService;
        this.loggingService = loggingService;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        permissionValidationService.validateForGlobal(Permission.ADMIN);

        Matcher m = PATH_INFO.matcher(req.getPathInfo());
        if (!m.matches()) {
            resp.sendError(400, "Example usage: http://localhost:7990/stash/plugins/servlet/users-with/LICENSED_USER");
            return;
        }

        Permission permission;
        try {
            permission = Permission.valueOf(m.group(1));
        } catch (IllegalArgumentException e) {
            resp.sendError(400, "Allowed permission types: " + Lists.newArrayList(Permission.values()));
            return;
        }

        resp.setContentType("text/plain");
        PrintWriter writer = resp.getWriter();

        String originalLevel = loggingService.getLevel(PERMISSION_SERVICE_LOGGER);
        try {
            loggingService.setLevel(PERMISSION_SERVICE_LOGGER, "TRACE");
            for (String user : permissionService.getUsersWithPermission(permission)) {
                writer.println(user);
            }
        } finally {
            loggingService.setLevel(PERMISSION_SERVICE_LOGGER, originalLevel);
        }
    }

}
